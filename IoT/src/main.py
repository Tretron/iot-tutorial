"""
Blynk is a platform with iOS and Android apps to control
Arduino, Raspberry Pi and the likes over the Internet.
You can easily build graphic interfaces for all your
projects by simply dragging and dropping widgets.

  Downloads, docs, tutorials: http://www.blynk.cc
  Sketch generator:           http://examples.blynk.cc
  Blynk community:            http://community.blynk.cc
  Social networks:            http://www.fb.com/blynkapp
                              http://twitter.com/blynk_app

This example shows how to initialize your ESP8266/ESP32 board
and connect it to Blynk.

Don't forget to change WIFI_SSID, WIFI_PASS and BLYNK_AUTH ;)
"""
from kaku import switchkaku
from machine import Pin
import BlynkLib
import network
import machine
import dht
import utime
import _thread




WIFI_SSID = 'labnet'
WIFI_PASS = 'smrsmrsmr'

BLYNK_AUTH = 'ahZA8aljbDxRlGt-Kx3xRZZsTBIHOW2O'

print("Connecting to WiFi...")
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.connect(WIFI_SSID, WIFI_PASS)
while not wifi.isconnected():
    pass

print('IP:', wifi.ifconfig()[0])

print("Connecting to Blynk...")
blynk = BlynkLib.Blynk(BLYNK_AUTH)

@blynk.on("connected")
def blynk_connected(ping):
    print('Blynk ready. Ping:', ping, 'ms')


LED = Pin(2, Pin.OUT)
SWITCH_ID = 6314
sending_pin = 17
retry = 20

def klik_aan():
    switchkaku(sending_pin, SWITCH_ID, True, retry)

def klik_uit():
    switchkaku(sending_pin, SWITCH_ID, False, retry)

@blynk.on("V*")
def blynk_handle_vpins(pin, value):

    print("V{} value: {}".format(pin, value[0]))
    led_value = 0
    if int(pin) == 4:
        if int(value[0]) == 1:
            LED.on()
            led_value = 255
        else:
            LED.off()
            led_value = 0
    if int(pin) == 3:
        if int(value[0]) == 1:
            klik_aan()
            print("KAKU aan")
        else:
            klik_uit()
            print("KAKU UIT")
        utime.sleep(0.1)
    blynk.virtual_write(0, led_value)

def runLoop():
    temp = 0
    d = dht.DHT11(machine.Pin(4))
    while True:
        d.measure()
        temp = d.temperature()
        hum = d.humidity()
        blynk.virtual_write(1, temp)
        blynk.virtual_write(2, hum)
        blynk.run()
        utime.sleep(1)

runLoop()






