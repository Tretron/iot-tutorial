from time import sleep
from kaku import switchkaku
from machine import Pin, ADC
import machine

SWITCH_ID = 6314
sending_pin = 17
adc = ADC(Pin(39))
p2 = Pin(2, Pin.OUT)    # create output pin on GPIO0

while True:
        if adc.read() < 3000:
            print("its to dark outthere")
            p2.on()
            switchkaku(sending_pin,SWITCH_ID,True,4)
        else:
            print("its to bright out there")
            p2.off()
            switchkaku(sending_pin,SWITCH_ID,False,4)
        print(adc.read())