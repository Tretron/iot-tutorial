from machine import Pin, ADC

adc = ADC(Pin(39))
p2 = Pin(2, Pin.OUT)    # create output pin on GPIO0

print(adc.read())

while True:
    if adc.read() < 3000:
        p2.on()
    else:
        p2.off()
    


