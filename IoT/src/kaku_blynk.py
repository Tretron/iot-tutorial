"""
Blynk is a platform with iOS and Android apps to control
Arduino, Raspberry Pi and the likes over the Internet.
You can easily build graphic interfaces for all your
projects by simply dragging and dropping widgets.

  Downloads, docs, tutorials: http://www.blynk.cc
  Sketch generator:           http://examples.blynk.cc
  Blynk community:            http://community.blynk.cc
  Social networks:            http://www.fb.com/blynkapp
                              http://twitter.com/blynk_app

This example shows how to initialize your ESP8266/ESP32 board
and connect it to Blynk.

Don't forget to change WIFI_SSID, WIFI_PASS and BLYNK_AUTH ;)
"""
from kaku import switchkaku
from machine import Pin
import BlynkLib
import network
import machine
import dht
import utime
import _thread




WIFI_SSID = 'labnet'
WIFI_PASS = 'smrsmrsmr'

BLYNK_AUTH = 'ahZA8aljbDxRlGt-Kx3xRZZsTBIHOW2O'

print("Connecting to WiFi...")
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.connect(WIFI_SSID, WIFI_PASS)
while not wifi.isconnected():
    pass

print('IP:', wifi.ifconfig()[0])

print("Connecting to Blynk...")
blynk = BlynkLib.Blynk(BLYNK_AUTH)

@blynk.on("connected")
def blynk_connected(ping):
    print('Blynk ready. Ping:', ping, 'ms')


SWITCH_ID = 6314
sending_pin = 17
retry = 40

def klik_aan():
    switchkaku(sending_pin, SWITCH_ID, True, retry)

def klik_uit():
    switchkaku(sending_pin, SWITCH_ID, False, retry)

@blynk.on("V3") #Function for reading out the in-app button
def v3_handler(value):
    if (int(value[0]) == 1):
        klik_aan()
        print("klik aan")
    else:
        klik_uit()
        print("klik uit")

def runLoop():
    while True:
        blynk.run()
    machine.idle()

runLoop()
