import utime

start = utime.ticks_ms()
utime.sleep_ms(1000)
duration = utime.ticks_ms() - start

print("Sleep took {}ms".format(duration))